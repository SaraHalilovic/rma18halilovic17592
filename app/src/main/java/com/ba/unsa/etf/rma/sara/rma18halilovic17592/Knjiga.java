package com.ba.unsa.etf.rma.sara.rma18halilovic17592;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.URL;
import java.util.ArrayList;

public  class Knjiga implements Parcelable{
    private String id;
    private String naziv;
    private String naziv_Autora;
    private ArrayList<Autor> autori=new ArrayList<>();
    private String opis;
    private String datumObjavljivanja;
    private String zanr;
    private URL slika;
    private int brojStranica;
    private int pregledana=0;



    public Knjiga(String id1, String naziv1,ArrayList<Autor>a, String opis1,String datum,URL slikica,int brojStranica1){
     setId(id1);
     setNaziv(naziv1);
     setAutori(a);
     setOpis(opis1);
     setDatumObjavljivanja(datum);
     setSlika(slikica);
     setBrojStranica(brojStranica1);

    }
    public Knjiga(String id1, String naziv1,ArrayList<Autor>a, String opis1,String datum,int brojStranica1){
        setId(id1);
        setNaziv(naziv1);
        setAutori(a);
        setOpis(opis1);
        setDatumObjavljivanja(datum);
       // setSlika("nema slike");
        setBrojStranica(brojStranica1);

    }


    public Knjiga(String A, String K, String z){
        naziv_Autora=A;
        naziv=K;
        zanr=z;
        setBrojStranica(0);
        setSlika(null);
        setDatumObjavljivanja("");
        setOpis("");
        setId("");
    }

    public Knjiga(){}
    public Knjiga (Parcel in){
        naziv_Autora=in.readString();
        naziv=in.readString();
        zanr=in.readString();
        brojStranica=in.readInt();
        opis=in.readString();
        datumObjavljivanja=in.readString();
        //autori=in.readParcelableArray();
        id=in.readString();


    }

    public static final Creator<Knjiga>CREATOR=new Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel in) {
            return new Knjiga(in);
        }

        @Override
        public Knjiga[] newArray(int size) {
            return new Knjiga[size];
        }
    };


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Autor> getAutori() {
        return autori;
    }

    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }

    public String getNaziv_Autora() {
        return naziv_Autora;
    }

    public void setNaziv_Autora(String naziv_Autora) {
        this.naziv_Autora = naziv_Autora;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv_Knjige) {
        this.naziv = naziv_Knjige;
    }

    public String getZanr() {
        return zanr;
    }

    public void setZanr(String zanr) {
        this.zanr = zanr;
    }


    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public URL getSlika() {
        return slika;
    }

    public void setSlika(URL slika) {
        this.slika = slika;
    }

    public int getBrojStranica() {
        return brojStranica;
    }

    public void setBrojStranica(int brojStranica) {
        this.brojStranica = brojStranica;
    }

    /*public void DajStranice(){

    }*/
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv_Autora);
        dest.writeString(naziv);
        dest.writeString(zanr);
        dest.writeString(id);
        dest.writeString(opis);
        dest.writeString(datumObjavljivanja);




    }

    public int getPregledana() {
        return pregledana;
    }

    public void setPregledana(int pregledana) {
        this.pregledana = pregledana;
    }
}
