package com.ba.unsa.etf.rma.sara.rma18halilovic17592;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListeFragment.OnItemClick1} interface
 * to handle interaction events.
 */
public class DodavanjeKnjigeFragment extends Fragment {

    ArrayList<String> kategorijice=new ArrayList<>();
   public static ArrayList<Knjiga> knjige1=new ArrayList<>();
    ArrayList<Autor> autori1=new ArrayList<>();

    Button dugme_UpisiKnjigu;
    EditText Autor;
    EditText Knjigaa;
    ImageView naslovna_slikica;
    Spinner spiner_kategorije;
    Button dugme_ponisti;
    Button galerija;
    private ListeFragment.OnItemClick1 oic;

    public DodavanjeKnjigeFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
   /* public static ListeFragment newInstance(String param1, String param2) {
        ListeFragment fragment = new ListeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_dodavanje_knjige, container, false);
        dugme_UpisiKnjigu=(Button)v.findViewById(R.id.dUpisiKnjigu);
        Autor=(EditText) v.findViewById(R.id.eAutor);
        Knjigaa=(EditText)v.findViewById(R.id.eNaziv);
        naslovna_slikica=(ImageView)v.findViewById(R.id.eNaslovna);
        galerija = (Button)v.findViewById(R.id.dNadjiSliku);
        spiner_kategorije=(Spinner)v.findViewById(R.id.sKategorijaKnjige);
        dugme_ponisti=(Button) v.findViewById(R.id.dPonisti);

        return v;
    }

/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemClick) {
            oic = (OnItemClick) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnItemClick");
        }
    }*/

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if (getArguments() != null) {
            if (getArguments().containsKey("arraylist")) {
                kategorijice = getArguments().getStringArrayList("arraylist");

            }
            if (getArguments().containsKey("knjige")) {
               knjige1 = getArguments().getParcelableArrayList("knjige");

            }

            if (getArguments().containsKey("autori")) {
                autori1 = getArguments().getParcelableArrayList("autori");

            }


        }

        final ArrayAdapter<String> s_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kategorijice);
        s_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spiner_kategorije.setAdapter(s_adapter);

        dugme_ponisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListeFragment nextFrag= new ListeFragment();
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("arraylist", kategorijice);
                bundle.putParcelableArrayList("knjige",knjige1);
                bundle.putParcelableArrayList("autori",autori1);
                nextFrag.setArguments(bundle);
                getActivity().getFragmentManager().beginTransaction()
                        .replace(R.id.FragmentPocetak, nextFrag,"findThisFragment")
                        .addToBackStack(null)
                        .commit();
            }
        });


       galerija.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i = new Intent(Intent.ACTION_PICK,
                       android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
               final int ACTIVITY_SELECT_IMAGE = 1234;
               startActivityForResult(i, ACTIVITY_SELECT_IMAGE);
           }
       });

        dugme_UpisiKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Autor.getText().toString().equals("") || Knjigaa.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "!!", Toast.LENGTH_SHORT).show();
                } else {

                    Knjiga knjigica = new Knjiga(Autor.getText().toString(), Knjigaa.getText().toString(), spiner_kategorije.getSelectedItem().toString());
                    Log.i("velicina", String.valueOf(knjige1.size()));
                    boolean pronadjena = false;
                    for (int u = 0; u < knjige1.size(); u++) {
                        if (knjige1.get(u).getNaziv_Autora().equals(knjigica.getNaziv_Autora()) && knjige1.get(u).getNaziv().equals(knjigica.getNaziv()) && knjige1.get(u).getZanr().equals(knjigica.getZanr())) {
                            pronadjena = true;
                        }
                    }

                    boolean d=KategorijeAkt.help.imaUBazi(knjigica);
                    if ( d==true) {
                        Log.i("da li", String.valueOf(knjige1.contains(knjigica)));
                        knjige1.add(knjigica);
                        KategorijeAkt.help.DodajKnjigu(knjigica);
                        int t = 0;
                        for (int i = 0; i < autori1.size(); i++) {
                            if (autori1.get(i).getImeiPrezime().equals(knjigica.getNaziv_Autora())) {
                                t++;
                            }

                        }

                        if (t == 0) autori1.add(new Autor(knjigica.getNaziv_Autora(), 1));

                        for (int i = 0; i < autori1.size(); i++) {
                            t = 0;
                            for (int j = 0; j < knjige1.size(); j++) {
                                if (autori1.get(i).getImeiPrezime().equals(knjige1.get(j).getNaziv_Autora())) {
                                    t++;
                                }
                                autori1.get(i).setBr_knjiga(t);
                                autori1.get(i).setBr_knjiga(t);

                            }
                        }


                        ListeFragment nextFrag = new ListeFragment();
                        Bundle bundle = new Bundle();
                        bundle.putStringArrayList("arraylist", kategorijice);
                        bundle.putParcelableArrayList("knjige", knjige1);
                        bundle.putParcelableArrayList("autori", autori1);
                        nextFrag.setArguments(bundle);
                        getActivity().getFragmentManager().beginTransaction()
                                .replace(R.id.FragmentPocetak, nextFrag, "findThisFragment")
                                .addToBackStack(null)
                                .commit();

                    }

                    ListeFragment nextFrag= new ListeFragment();
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("arraylist", kategorijice);
                    bundle.putParcelableArrayList("knjige",knjige1);
                    bundle.putParcelableArrayList("autori",autori1);
                    nextFrag.setArguments(bundle);
                    getActivity().getFragmentManager().beginTransaction()
                            .replace(R.id.FragmentPocetak, nextFrag,"findThisFragment")
                            .addToBackStack(null)
                            .commit();
                }
            }
        });


    }
        public interface OnItemClick {
            // TODO: Update argument type and name
            void onItemClicked(int pos);
        }

    }