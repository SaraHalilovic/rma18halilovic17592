package com.ba.unsa.etf.rma.sara.rma18halilovic17592;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;
import static com.ba.unsa.etf.rma.sara.rma18halilovic17592.ListeFragment.knjige;



/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListeFragment.OnItemClick1} interface
 * to handle interaction events.
 */



public class FragmentOnline extends Fragment implements DohvatiKnjige.IDohvatiKnjigeDone,DohvatiNajnovije.IDohvatiNajnovijeDone​,MyReceiver.Receiver {


    ArrayList<String> kategorijice=new ArrayList<>();

    ArrayList<Autor> autori1=new ArrayList<>();


    ArrayList<Knjiga> knjige1=new ArrayList<>();

    ArrayList<Knjiga> knjizice=new ArrayList<>();
    ArrayList<Knjiga> knjizice1=new ArrayList<>();
    ArrayList<String> unosi=new ArrayList<>();
    Button dugme_dodaj;
    Button dugme_run;
    Button dugme_natrag;
    Spinner skategorije,srezultat;
    EditText upit;
    ArrayList<String> naslovi=new ArrayList<>();

    private ListeFragment.OnItemClick1 oic;

    public FragmentOnline() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
   /* public static ListeFragment newInstance(String param1, String param2) {
        ListeFragment fragment = new ListeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_online, container, false);
        dugme_natrag=(Button)v.findViewById(R.id.dPovratak);
        dugme_dodaj=(Button)v.findViewById(R.id.dAdd);
        dugme_run=(Button)v.findViewById(R.id.dRun);
        upit=(EditText)v.findViewById(R.id.tekstUpit);
        skategorije=(Spinner) v.findViewById(R.id.sKategorije);
        srezultat=(Spinner) v.findViewById(R.id.sRezultat);


        return v;
    }

/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemClick) {
            oic = (OnItemClick) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnItemClick");
        }
    }*/

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            if (getArguments().containsKey("arraylist")) {
                kategorijice = getArguments().getStringArrayList("arraylist");

            }
            if (getArguments().containsKey("knjige")) {
                knjige1 = getArguments().getParcelableArrayList("knjige");

            }

            if (getArguments().containsKey("autori")) {
                autori1 = getArguments().getParcelableArrayList("autori");

            }


        }

            ArrayAdapter<String> adapter_kat = new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_item,kategorijice);
            skategorije.setAdapter(adapter_kat);



            dugme_dodaj.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {


                                                   String naslovKnjige = srezultat.getSelectedItem().toString();

                                                       Knjiga knjigica;
                                                       String pisac = "";
                                                       for (int p = 0; p < knjizice.size(); p++) {
                                                           if (naslovKnjige.equals(knjizice.get(p).getNaziv())) {
                                                               knjigica = new Knjiga(knjizice.get(p).getId(), knjizice.get(p).getNaziv(), knjizice.get(p).getAutori(), knjizice.get(p).getOpis(), knjizice.get(p).getDatumObjavljivanja(), knjizice.get(p).getSlika(), knjizice.get(p).getBrojStranica());
                                                               if(knjizice.get(p).getAutori().size()>0){
                                                               pisac = knjizice.get(p).getAutori().get(0).getImeiPrezime();
                                                               for (int osa = 1; osa < knjizice.get(p).getAutori().size(); osa++) {
                                                                   pisac = pisac + "," + knjizice.get(p).getAutori().get(osa).getImeiPrezime();
                                                               }
                                                               knjigica.setNaziv_Autora(pisac);}
                                                               else{
                                                                   knjigica.setNaziv_Autora("");
                                                               }
                                                               // knjigica.setNaziv(naslovKnjige);
                                                               knjigica.setZanr(skategorije.getSelectedItem().toString());
                                                               boolean ima = false;
                                                               for (int k = 0; k < knjige1.size(); k++) {
                                                                   if (knjigica.getNaziv().equals(knjige1.get(k).getNaziv()) && knjigica.getNaziv_Autora().equals(knjige1.get(k).getNaziv_Autora()) && knjigica.getZanr().equals(knjige1.get(k).getZanr()) && knjigica.getId().equals(knjige1.get(k).getId())) {
                                                                       ima = true;
                                                                       Log.i("sada",String.valueOf(ima));
                                                                   }
                                                               }
                                                               boolean d=KategorijeAkt.help.imaUBazi(knjigica);

                                                               if ( d==true) {
                                                                   knjige1.add(knjigica);
                                                                   KategorijeAkt.help.DodajKnjigu(knjigica);


                                                                   int t = 0;
                                                                   for (int i = 0; i < autori1.size(); i++) {
                                                                       if (autori1.get(i).getImeiPrezime().equals(knjigica.getNaziv_Autora())) {
                                                                           autori1.get(i).getKnjige().add(knjigica.getId());
                                                                           t++;
                                                                       }

                                                                   }
                                                                    // dopisuje  autora
                                                                   if (t == 0) {
                                                                       ArrayList<String> a = new ArrayList<>();
                                                                       a.add(knjigica.getId());
                                                                       autori1.add(new Autor(knjigica.getNaziv_Autora(), a));

                                                                   }
                                                                   //broji knjige
                                                                   for (int i = 0; i < autori1.size(); i++) {
                                                                       t = 0;
                                                                       for (int j = 0; j < knjige1.size(); j++) {
                                                                           if (autori1.get(i).getImeiPrezime().equals(knjige1.get(j).getNaziv_Autora())) {
                                                                               t++;
                                                                           }
                                                                           autori1.get(i).setBr_knjiga(t);
                                                                           // autori1.get(i).setBr_knjiga(autori1.get(i).getKnjige().size());
                                                                       }
                                                                   }

                                                                   break;
                                                               }

                                                           }
                                                       }


                                                       ListeFragment nextFrag = new ListeFragment();
                                                       Bundle bundle = new Bundle();
                                                       bundle.putStringArrayList("arraylist", kategorijice);
                                                       bundle.putParcelableArrayList("knjige", knjige1);
                                                       bundle.putParcelableArrayList("autori", autori1);
                                                       nextFrag.setArguments(bundle);
                                                       getActivity().getFragmentManager().beginTransaction()
                                                               .replace(R.id.FragmentPocetak, nextFrag, "findThisFragment")
                                                               .addToBackStack(null)
                                                               .commit();

                                               }
                                           }


            );

            dugme_run.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    naslovi.clear();
                    String trazi = upit.getText().toString();

                    if(upit.getText().toString().length()>9 && upit.getText().toString().substring(0,9).equals("korisnik:")){
                        String ostatak=upit.getText().toString().substring(9);
                        Intent intent=new Intent(Intent.ACTION_SYNC,null,getActivity(),KnjigePoznanika.class);
                        MyReceiver mReceiver=new MyReceiver(new Handler());
                        mReceiver.setReceiver(FragmentOnline.this);
                        intent.putExtra("idkorisnika",ostatak);
                        intent.putExtra("receiver",mReceiver);
                       // getActivity().startService(intent);

                    }
                      else if(upit.getText().toString().length()>6 && upit.getText().toString().substring(0,6).equals("autor:")){
                      String ostatak=upit.getText().toString().substring(6);
                      String[] trazi1 = ostatak.split(":");
                      for(String h:trazi1){
                          Log.i(TAG,"bb" + ostatak);
                          new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone​) FragmentOnline.this).execute(h);}
                  }

                      else {
                       String[] trazi1 = upit.getText().toString().split(";");
                       for(String h:trazi1){
                           //Log.i(TAG,"onako");
                           new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) FragmentOnline.this).execute(h);}


                }
            }});



            dugme_natrag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*Vrati na pocetni fragment*/
                    getFragmentManager().popBackStack();
                }
            });


        }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onDohvatiDone(ArrayList<Knjiga> rez) {

        boolean ima=false;
        for(int i=0;i<rez.size();i++) {
            /*Da li postoje vec*/

            for (int k = 0; k < naslovi.size(); k++) {
                if (naslovi.get(k).equals(rez.get(i).getNaziv())) {
                    ima = true;
                }
            }
            if (!ima) {
                naslovi.add(rez.get(i).getNaziv());
                knjizice.add(rez.get(i));
                ima=false;
            }
        }
        // SVe nazive iz knjige unijeti u spiner rezultat
       //   naslovi.clear();
       knjizice.addAll(rez);
        ArrayAdapter<String> adapter_naslovi=new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,naslovi);
        srezultat.setAdapter(adapter_naslovi);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onNajnovijeDone(ArrayList<Knjiga> rez) {
        onDohvatiDone(rez);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

    }


    public interface OnItemClick1 {
        // TODO: Update argument type and name
        void onItemClicked1(int pos);
    }
}
