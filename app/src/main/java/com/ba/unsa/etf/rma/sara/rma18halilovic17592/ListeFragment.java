package com.ba.unsa.etf.rma.sara.rma18halilovic17592;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.ba.unsa.etf.rma.sara.rma18halilovic17592.BazaOpenHelper.KATEGORIJE_Tabela;
import static com.ba.unsa.etf.rma.sara.rma18halilovic17592.BazaOpenHelper.naziv_Kategorije;
import static com.ba.unsa.etf.rma.sara.rma18halilovic17592.BazaOpenHelper.naziv_Knjige;
import static com.ba.unsa.etf.rma.sara.rma18halilovic17592.KategorijeAkt.help;
import static com.ba.unsa.etf.rma.sara.rma18halilovic17592.KategorijeAkt.siriL;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListeFragment.OnItemClick1} interface
 * to handle interaction events.
  */
public class ListeFragment extends Fragment {

    ArrayList<String> unosi=new ArrayList<>();
   public static ArrayList<Knjiga> knjige=new ArrayList<>();
    ArrayList<Autor> autori=new ArrayList<>();


    EditText pretraga_tekst;
    Button dugme_pretraga;
    Button dugme_dodajKategoriju;
    Button dugme_dodajKnjigu;
    Button dugme_autori;
    Button dugme_kategorije;
    ListView lista_kategorija;
    Button dugme_onlineKnjige;
    private OnItemClick1 oic;
    private int p=1;
    public ListeFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
   /* public static ListeFragment newInstance(String param1, String param2) {
        ListeFragment fragment = new ListeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_liste_fragmenti, container, false);

         pretraga_tekst=(EditText)v.findViewById(R.id.tekstPretraga);
         dugme_pretraga=(Button) v.findViewById(R.id.dPretraga);
         dugme_dodajKategoriju=(Button) v.findViewById(R.id.dDodajKategoriju);
         dugme_dodajKnjigu=(Button) v.findViewById(R.id.dDodajKnjigu);
         dugme_onlineKnjige=(Button) v.findViewById(R.id.dDodajOnline);
         dugme_kategorije=(Button) v.findViewById(R.id.dKategorije);
         dugme_autori=(Button) v.findViewById(R.id.dAutori);
         lista_kategorija=(ListView) v.findViewById(R.id.listaKategorija);

       /* if(unosi.size()==0){
             unosi.add(getString(R.string.Roman));
         unosi.add("SCI FI");}*/
        unosi.clear();
        unosi.addAll(KategorijeAkt.help.dajKategorije());
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemClick1) {
            oic = (OnItemClick1) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OOnItemClick");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Knjiga mala_knjigica=new Knjiga();

        if(getArguments()!=null) {
            if (getArguments().containsKey("arraylist")) {
                unosi = getArguments().getStringArrayList("arraylist");

            }
            if (getArguments().containsKey("knjige")) {
             knjige = getArguments().getParcelableArrayList("knjige");

            }
            if (getArguments().containsKey("autori")) {
             autori = getArguments().getParcelableArrayList("autori");

            }
        }

        if(getArguments().containsKey("Alista")) {
            knjige = getArguments().getParcelableArrayList("Alista");
        }

        final ArrayAdapter adapter;
        if(unosi.size()==0) {
            unosi.add(getString(R.string.Roman));
            unosi.add("SCI FI");
        }


        adapter= new ArrayAdapter<>(getActivity(),android.R.layout.simple_expandable_list_item_1,unosi);
        dugme_dodajKategoriju.setEnabled(false);



                dugme_pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lista_kategorija.setAdapter(adapter);
                String pomocni = pretraga_tekst.getText().toString();
                adapter.getFilter().filter(pomocni, new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int c) {
                        if (c != 0) dugme_dodajKategoriju.setEnabled(false);
                        else dugme_dodajKategoriju.setEnabled(true);

                    }});
                adapter.notifyDataSetChanged();
            }});


        dugme_dodajKategoriju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* lista_kategorija.setAdapter(adapter);
                unosi.add(pretraga_tekst.getText().toString());
                String naziv_kategorijice1=pretraga_tekst.getText().toString();
                adapter.clear();
                adapter.addAll(unosi);
                adapter.notifyDataSetChanged();
                pretraga_tekst.setText("");
                dugme_dodajKategoriju.setEnabled(false);*/
                p=-1;

                lista_kategorija.setAdapter(adapter);
                String naziv_kategorijice1=pretraga_tekst.getText().toString();
                long e= KategorijeAkt.help.dodajKategoriju(naziv_kategorijice1);
                unosi.clear();
                unosi.addAll(KategorijeAkt.help.dajKategorije());
                adapter.clear();
                adapter.addAll(unosi);
                adapter.notifyDataSetChanged();
                pretraga_tekst.setText("");
                dugme_dodajKategoriju.setEnabled(false);




            }
        });

        dugme_onlineKnjige.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               FragmentOnline nextFrag= new FragmentOnline();
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("arraylist", unosi);
                bundle.putParcelableArrayList("knjige",knjige);
                bundle.putParcelableArrayList("autori",autori);
                nextFrag.setArguments(bundle);


                    getActivity().getFragmentManager().beginTransaction()
                            .replace(R.id.FragmentPocetak, nextFrag, "findThisFragment")
                            .addToBackStack(null)
                            .commit();


            }
        });


        lista_kategorija.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               if(p==7777){
                String k = lista_kategorija.getItemAtPosition(position).toString();

                //novu listu po kategorijama
                ArrayList<Knjiga> kategorijice1 = new ArrayList<>();
                for (int i = 0; i < knjige.size(); i++) {
                    if (k.equals(knjige.get(i).getZanr())) {
                        kategorijice1.add(knjige.get(i));
                    }
                }
                KnjigeFragment nextFrag = new KnjigeFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("pisac", kategorijice1);
                nextFrag.setArguments(bundle);

                if(siriL==false) {
                    getActivity().getFragmentManager().beginTransaction()
                            .replace(R.id.FragmentPocetak, nextFrag, "findThisFragment")
                            .addToBackStack(null)
                            .commit();
                }
                else{
                    getActivity().getFragmentManager().beginTransaction()
                            .replace(R.id.mjestoF2, nextFrag, "findThisFragment")
                            .addToBackStack(null)
                            .commit();
                }

            }
            if(p==8888){

                TextView textView = (TextView) view.findViewById(R.id.ImenaA);;
                String naziv=textView.getText().toString();

                ArrayList<Knjiga> kategorijice1 = new ArrayList<>();
                for (int i = 0; i < knjige.size(); i++) {
                    if (naziv.equals(knjige.get(i).getNaziv_Autora())) {
                        kategorijice1.add(knjige.get(i));
                    }
                }
                KnjigeFragment nextFrag = new KnjigeFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("pisac", kategorijice1);
                nextFrag.setArguments(bundle);

                if(siriL==false) {
                    getActivity().getFragmentManager().beginTransaction()
                            .replace(R.id.FragmentPocetak, nextFrag, "findThisFragment")
                            .addToBackStack(null)
                            .commit();
                }
                else{
                    getActivity().getFragmentManager().beginTransaction()
                            .replace(R.id.mjestoF2, nextFrag, "findThisFragment")
                            .addToBackStack(null)
                            .commit();
                }

            }
            }
        })
                ;
        dugme_dodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DodavanjeKnjigeFragment nextFrag= new DodavanjeKnjigeFragment();
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("arraylist", unosi);
                bundle.putParcelableArrayList("knjige",knjige);
                bundle.putParcelableArrayList("autori",autori);
                nextFrag.setArguments(bundle);
                if(siriL){
                    getActivity().getFragmentManager().beginTransaction()
                            .replace(R.id.mjestoF3, nextFrag, "findThisFragment")
                            .addToBackStack(null)
                            .commit();
                }
                else {
                    getActivity().getFragmentManager().beginTransaction()
                            .replace(R.id.FragmentPocetak, nextFrag, "findThisFragment")
                            .addToBackStack(null)
                            .commit();
                }


        }});

        dugme_kategorije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  MyListAdapter adapter2 = new MyListAdapter(getActivity(), knjige);

                 ArrayAdapter adapter1= new ArrayAdapter<>(getActivity(),android.R.layout.simple_expandable_list_item_1,unosi);
                 lista_kategorija.setAdapter(adapter1);
                dugme_pretraga.setVisibility(View.VISIBLE);
                dugme_dodajKategoriju.setVisibility(View.VISIBLE);
                pretraga_tekst.setVisibility(View.VISIBLE);
                p=7777;

            }
        });

        dugme_autori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Adapter_autori adapter2 = new Adapter_autori(getActivity(), autori);
                lista_kategorija.setAdapter(adapter2);

                dugme_pretraga.setVisibility(View.GONE);
                dugme_dodajKategoriju.setVisibility(View.GONE);
                pretraga_tekst.setVisibility(View.GONE);
                p=8888;
            }
        });

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnItemClick1 {
        // TODO: Update argument type and name
        void onItemClicked1(int pos);
    }


}
