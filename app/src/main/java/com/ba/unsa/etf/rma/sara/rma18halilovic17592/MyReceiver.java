package com.ba.unsa.etf.rma.sara.rma18halilovic17592;


import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class MyReceiver extends ResultReceiver {

    private Receiver mReciever;

    public MyReceiver(Handler handler) {
        super(handler);
    }

    public interface Receiver {
        public void onReceiveResult(int resultCode, Bundle resultData);
    }

    public void setReceiver(Receiver reciever) {
        mReciever = reciever;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if(mReciever != null) {
            mReciever.onReceiveResult(resultCode,resultData);
        }
    }
}
