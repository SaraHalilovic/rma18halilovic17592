package com.ba.unsa.etf.rma.sara.rma18halilovic17592;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListeFragment.OnItemClick1} interface
 * to handle interaction events.
 */
public class KnjigeFragment extends Fragment {

    ArrayList<String> kategorijice=new ArrayList<>();
    ArrayList<Knjiga> knjige1=new ArrayList<>();
    ArrayList<Autor> autori1=new ArrayList<>();
    ListView lista;
    Button dugme_natrag;

    private ListeFragment.OnItemClick1 oic;

    public KnjigeFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
   /* public static ListeFragment newInstance(String param1, String param2) {
        ListeFragment fragment = new ListeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_knjige, container, false);
        dugme_natrag=(Button)v.findViewById(R.id.dPovratak);
        lista=(ListView)v.findViewById(R.id.listaKnjiga);

        return v;
    }

/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemClick) {
            oic = (OnItemClick) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnItemClick");
        }
    }*/

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if (getArguments() != null) {

            if (getArguments().containsKey("pisac")) {
                knjige1 = getArguments().getParcelableArrayList("pisac");

            }


            }

            MyListAdapter adapter2 = new MyListAdapter(getActivity(), knjige1);
            lista.setAdapter(adapter2);

            dugme_natrag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*Vrati na pocetni fragment*/
                    getFragmentManager().popBackStack();
                }
            });


        }

    public interface OnItemClick1 {
        // TODO: Update argument type and name
        void onItemClicked1(int pos);
    }

}