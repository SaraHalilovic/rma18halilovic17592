package com.ba.unsa.etf.rma.sara.rma18halilovic17592;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.facebook.stetho.inspector.database.SqliteDatabaseDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class BazaOpenHelper extends SQLiteOpenHelper {


    public static final String DATABASE_NAME="mojaBaza1.db";
    public static final int DATABASE_VERSION=1;


    /* Kategorija​ sa dvije kolone: ​_id​ - primarni ključ i kolona ​naziv​ tipa TEXT */
    public static final String KATEGORIJE_Tabela="Kategorija";
    public static final String id_Kategorije="_id";
    public static final String naziv_Kategorije="naziv";
    public static final String KATEGORIJE_CREATE="create table "+ KATEGORIJE_Tabela
            + "("+id_Kategorije+ " integer primary key autoincrement, "+ naziv_Kategorije+" text not null );";


    /*Knjiga​ sa kolonama: ​_id​ - primarni ključ, ​naziv​ tipa TEXT,
    ​opis​ tipa TEXT, datumObjavljivanja​ tipa TEXT, ​brojStranica​ tipa INTEGER, ​idWebServis​ tipa TEXT koja predstavlja id sa web servisa,
    ​idkategorije​ tipa INTEGER, ​slika ​tipa TEXT(url slike) i pregledana​ tipa INTEGER(1 za kada je kliknuto na navedenu knjigu, 0 inače)*/

    public static final String KNJIGA_Tabela="Knjiga";
    public static final String id_Knjige="_id";
    public static final String naziv_Knjige="naziv";
    public static final String opis_Knjige="opis";
    public static final String datumObjavljivanja_Knjige="datumObjavljivanja";
    public static final String brojStranica_Knjige="brojStranica";
    public static final String idWebServis_Knjige="idWebServis";
    public static final String idKategorije_Knjige="idkategorije";
    public static final String slika_Knjige="slika";
    public static final String pregledana_Knjige="pregledana";
    public static final String KNJIGE_CREATE="create table "+ KNJIGA_Tabela
            + "("+id_Knjige+ " integer primary key autoincrement, "+ naziv_Knjige +" text not null, " + opis_Knjige + " text, "+ datumObjavljivanja_Knjige+ " text, "
            + brojStranica_Knjige+ " integer, " + idWebServis_Knjige+" text, "+ idKategorije_Knjige+ " integer, " +slika_Knjige+ " text, "+ pregledana_Knjige+" integer );";


    /*Autor​ sa kolonama: ​_id​ - primarni ključ, ​ime​ tipa TEXT*/
    public static final String AUTOR_Tabela="Autor";
    public static final String id_Autora="_id";
    public static final String ime_Autora="ime";
    public static final String AUTOR_CREATE="create table " + AUTOR_Tabela + " ( " + id_Autora + " integer primary key autoincrement, " + ime_Autora +" TEXT);";



    /*Autorstvo​ sa kolonama: ​_id​ - primarni ključ, ​idautora​ tipa INTEGER i ​idknjige​ tipa INTEGER*/
    public static final String AUTORSTVO_Tabela="Autorstvo";
    public static final String id_Autorstvo="_id";
    public static final String idautora_Autorstvo="idautora";
    public static final String idknjige_Autorstvo="idknjige";
    public static final String AUTORSTVO_CREATE="create table " + AUTORSTVO_Tabela + " (" + id_Autorstvo + " integer primary key autoincrement, " + idautora_Autorstvo + " TEXT, "+ idknjige_Autorstvo+" TEXT);";


    public BazaOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context,name,factory,version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(KATEGORIJE_CREATE);
        db.execSQL(KNJIGE_CREATE);
        db.execSQL(AUTORSTVO_CREATE);
        db.execSQL(AUTOR_CREATE);
    }

     public long dodajKategoriju(String nazivK)  {
         SQLiteDatabase db = this.getWritableDatabase();
         long id=0;

       /*  String queryZaKategoriju = "SELECT * FROM " + BazaOpenHelper.KATEGORIJE_Tabela + " g " +
                 " WHERE g." + naziv_Kategorije + "=  '" + nazivK + "'" + " ";

         Cursor c = db.rawQuery(queryZaKategoriju, null);

         if (c == null) {*/
       try{
             ContentValues noveVrijednost = new ContentValues();
             noveVrijednost.put(naziv_Kategorije, nazivK);
             id=db.insertOrThrow(KATEGORIJE_Tabela,null,noveVrijednost);
             //db.insert(BazaOpenHelper.KATEGORIJE_Tabela, null, noveVrijednost);
           }
           catch (SQLiteConstraintException  e){
               return -1;
           }


         return id;
     }

     public ArrayList dajKategorije(){
         SQLiteDatabase db=this.getWritableDatabase();
         ArrayList<String> kategorijeBaza=new ArrayList<>();
         String queryZaKategorije = "SELECT " +naziv_Kategorije +" FROM " + BazaOpenHelper.KATEGORIJE_Tabela ;

         Cursor c= db.rawQuery(queryZaKategorije, null);


         if (c != null) {
             if (c.moveToFirst()) {
                 do {
                     String naziv1 = c.getString(c.getColumnIndex(naziv_Kategorije));
                     kategorijeBaza.add(naziv1);
                     } while (c.moveToNext());
             }
             //FragmentZanrovi.zanrovi=listaz;
         }

     return kategorijeBaza;
     }


     public long DodajKnjigu(Knjiga e){
         SQLiteDatabase db = this.getWritableDatabase();
         long idd=0;
         try{
             int a=0;
             int idKategorije=0;
             String queryzaK="SELECT " + id_Kategorije+ " FROM " + BazaOpenHelper.KATEGORIJE_Tabela + " WHERE " + naziv_Kategorije + "= '"+e.getZanr()+"';" ;
             Log.i("vidi:", queryzaK);
             Cursor id=db.rawQuery(queryzaK, null);

         if (id != null) {
             if (id.moveToFirst()) {
                 idKategorije=id.getInt(id.getColumnIndex(id_Kategorije));
}
             Log.i("vidi sad:",String.valueOf( idKategorije));
         }

             ContentValues noveVrijednost = new ContentValues();
             noveVrijednost.put(naziv_Knjige,e.getNaziv());
             noveVrijednost.put(opis_Knjige,e.getOpis());
             noveVrijednost.put(datumObjavljivanja_Knjige,e.getDatumObjavljivanja());
             noveVrijednost.put(brojStranica_Knjige,e.getBrojStranica());
             noveVrijednost.put(idWebServis_Knjige,e.getId());
             noveVrijednost.put(idKategorije_Knjige,idKategorije);
             if(e.getSlika()!=null){
             noveVrijednost.put(slika_Knjige,String.valueOf(e.getSlika()));}
             else {
                 noveVrijednost.put(slika_Knjige,"nema slike");
             }
             noveVrijednost.put(pregledana_Knjige,e.getPregledana());

              idd=db.insertOrThrow(KNJIGA_Tabela,null,noveVrijednost);

             //for petlja za sve autore knjige
             boolean autor=false;
             for(int i=0;i<e.getAutori().size();i++){
                 if(DaLiJeAutorUListi(e.getAutori().get(i).getImeiPrezime())){
                     //autor nije u tabelici
                     Log.i("samomalo",String.valueOf(DaLiJeAutorUListi(e.getAutori().get(i).getImeiPrezime())));
                     ContentValues vrijednost=new ContentValues();
                     vrijednost.put(ime_Autora,e.getAutori().get(i).getImeiPrezime());
                     long z=db.insertOrThrow(AUTOR_Tabela,null,vrijednost);
                 }
             }

             //Upisati id autora i id knjige u autorstvo

             //id knjige
             String upitzaknjigu="SELECT " + id_Knjige+ " FROM " + BazaOpenHelper.KNJIGA_Tabela + " WHERE "
                     + idWebServis_Knjige+"= '"+e.getId()+"' AND "+ opis_Knjige+"= '"+e.getOpis()+"' AND "+naziv_Knjige+"= '"+e.getNaziv()+"';" ;
             Log.i("hajdemala",upitzaknjigu);

             Cursor c1=db.rawQuery(upitzaknjigu, null);
             int idKnjig11=0;
             if (c1 != null) {
                 if (c1.moveToFirst()) {
                     idKnjig11=c1.getInt(id.getColumnIndex(id_Knjige));}}
                     c1.close();

             //id autora:
            // String queryAutor="SELECT " + id_Autora+ " FROM " + BazaOpenHelper.AUTOR_Tabela + " WHERE " + naziv_Kategorije + "= '"+e.getZanr()+"';" ;

             for(int i=0;i<e.getAutori().size();i++) {
                 String queryAutor="SELECT " + id_Autora+ " FROM " + BazaOpenHelper.AUTOR_Tabela + " WHERE " + ime_Autora + "= '"+e.getAutori().get(i).getImeiPrezime()+"';" ;
                 int ime=0;
                 Cursor aj=db.rawQuery(queryAutor,null);
                 if (aj != null) {
                     if (aj.moveToFirst()) {
                         ime=aj.getInt(aj.getColumnIndex(id_Autora));
                     }
                 }
                 aj.close();
                 ContentValues r=new ContentValues();
                 r.put(idknjige_Autorstvo,idKnjig11);
                 r.put(idautora_Autorstvo,ime);
                 long iddd=db.insertOrThrow(AUTORSTVO_Tabela,null,r);


             }



                 }
         catch (SQLiteConstraintException p){
             return -1;
         }

         return idd;
     }

     public boolean DaLiJeAutorUListi(String naziv){
        SQLiteDatabase db=getWritableDatabase();
        String upit="SELECT " +id_Autora + " FROM " +BazaOpenHelper.AUTOR_Tabela + " WHERE " + ime_Autora+ "= '"+naziv+"';";
         Cursor id=db.rawQuery(upit, null);

        try{
            if(id==null)
                return false;
        }
        catch(SQLiteConstraintException p){
            return true;
        }
        return true;
     }


    /*public ArrayList<Knjiga> knjigeKategorije(long idKategorije) throws MalformedURLException {
        SQLiteDatabase db=this.getWritableDatabase();
        ArrayList<Knjiga> knjigaBaza=new ArrayList<>();

        String upit="SELECT * FROM " +BazaOpenHelper.KNJIGA_Tabela+ ";";
        Cursor c=db.rawQuery(upit,null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    String naziv1 = c.getString(c.getColumnIndex(naziv_Knjige));
                    String opis = c.getString(c.getColumnIndex(opis_Knjige));
                    String datum = c.getString(c.getColumnIndex(datumObjavljivanja_Knjige));
                    int brStr = c.getInt(c.getColumnIndex(brojStranica_Knjige));
                    String slikica = c.getString(c.getColumnIndex(slika_Knjige));
                    URL slika = null;
                    if (slikica.equals("nema slike")) {
                        slika = null;
                    } else {
                        slika = new URL(slikica);
                    }

                    //dobavi autore

                    String upit2="SELECT FROM " +BazaOpenHelper.KNJIGA_Tabela+ ";";

                    // listar.add(z); listar.add(z2); listar.add(z3); listar.add(z4);
                    //listar.add(z5); listar.add(z6); listar.add(z7);
                } while (c.moveToNext());
            }
        }

        return knjigaBaza;
    }*/

    public boolean imaUBazi(Knjiga e){
        SQLiteDatabase db=getWritableDatabase();
        String t;
        if(e.getSlika()==null){
            t="nema slike";
        }
        else {
            t=String.valueOf(e.getSlika());
        }
        String upit="SELECT * FROM " + BazaOpenHelper.KNJIGA_Tabela + " WHERE "
                + idWebServis_Knjige+"= '"+e.getId()+"' AND "+ opis_Knjige+"= '"+e.getOpis()+"' " +
                "AND "+brojStranica_Knjige+"= '"+e.getBrojStranica()+"' AND "+ datumObjavljivanja_Knjige+"='"+e.getDatumObjavljivanja()+"' AND "+
                slika_Knjige+"= '"+t+"' AND "+naziv_Knjige+"= '"+e.getNaziv()+"';" ;

        Log.i("mozda",upit);

        Cursor id=db.rawQuery(upit, null);


            if(id==null)
                return false;

        return true;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ KATEGORIJE_Tabela);
        db.execSQL("DROP TABLE IF EXISTS "+ KNJIGA_Tabela);
        db.execSQL("DROP TABLE IF EXISTS "+ AUTORSTVO_Tabela);
        db.execSQL("DROP TABLE IF EXISTS "+ AUTOR_Tabela);

        onCreate(db);
    }

}
