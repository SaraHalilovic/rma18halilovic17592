package com.ba.unsa.etf.rma.sara.rma18halilovic17592;


import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListeFragment.OnItemClick1} interface
 * to handle interaction events.
 */
public class FragmentPreporuci extends Fragment {

    TextView naziv,autor,opis,br_str,datum;
    ImageView slikica;
    Spinner kontakti;
    Button posalji;
    String uzmiNaziv="";
    String uzmiPrvogAutora="";

    ArrayList<String> telefoncici_imena=new ArrayList<>();
    ArrayList<OsobaImenik> telefoncici=new ArrayList<>();
    ArrayList<String> telefoncici_email=new ArrayList<>();

    public FragmentPreporuci() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
   /* public static ListeFragment newInstance(String param1, String param2) {
        ListeFragment fragment = new ListeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v=inflater.inflate(R.layout.fragment_preporuci, container, false);

        naziv=v.findViewById(R.id.eNaziv1);
        autor=v.findViewById(R.id.eAutor1);
        opis=v.findViewById(R.id.eOpis1);
        br_str=v.findViewById(R.id.eBrojStranica1);
        datum=v.findViewById(R.id.eDatumObjavljivanja1);
        slikica= v.findViewById(R.id.eNaslovna1);
        kontakti=v.findViewById(R.id.sKontakti);
        posalji=v.findViewById(R.id.dPosalji);

        popuni_podatke();
        ArrayAdapter<String> adapter_kont = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,telefoncici_email);

        adapter_kont.sort(new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareTo(rhs);
            }
        });

        kontakti.setAdapter(adapter_kont);

        return v;



    }

/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemClick) {
            oic = (OnItemClick) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnItemClick");
        }
    }*/

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey("knjigica_mala")) {
                Knjiga book1 = getArguments().getParcelable("knjigica_mala");
                naziv.setText(book1.getNaziv());
                uzmiNaziv=naziv.getText().toString();
                String pisac="";
                if(book1.getAutori().size()>0){
                pisac=(book1.getAutori().get(0).getImeiPrezime());
                uzmiPrvogAutora=book1.getAutori().get(0).getImeiPrezime();
                for(int i=1;i<book1.getAutori().size();i++){
                   pisac+=(", "+ book1.getAutori().get(i).getImeiPrezime() );


                }
                }
                autor.setText(pisac);
                opis.setText(book1.getOpis());
                datum.setText(book1.getDatumObjavljivanja());
                br_str.setText(String.valueOf(book1.getBrojStranica()));

                if(book1.getSlika()!=null){
                    Picasso.get().load(book1.getSlika().toString()).into(slikica);}
                else{
                    Picasso.get().load(R.drawable.cikac1).into(slikica);
                }

            }



        }


        posalji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String imeZaPoruku;
                imeZaPoruku="";
                String odabrano_ime=kontakti.getSelectedItem().toString();
                for(int brojac=0;brojac<telefoncici.size();brojac++){
                    if(odabrano_ime.equals(telefoncici.get(brojac).getEmail())){
                        imeZaPoruku=telefoncici.get(brojac).getName();
                    }
                }

                Intent intentEmail=new Intent(Intent.ACTION_SEND);
                intentEmail.setType("plain/text");
                intentEmail.putExtra(Intent.EXTRA_EMAIL  , new String[]{odabrano_ime});
                intentEmail.putExtra(Intent.EXTRA_SUBJECT, "Preporuka knjige");
                intentEmail.putExtra(Intent.EXTRA_TEXT   , "Zdravo " + imeZaPoruku + ",\n" +"Pročitaj knjigu "+ uzmiNaziv+ " od " + uzmiPrvogAutora + "!");

                try {
                    startActivity(Intent.createChooser(intentEmail, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    public void popuni_podatke(){
        ContentResolver x=getActivity().getContentResolver();
        Cursor phones= x.query(ContactsContract.Contacts.CONTENT_URI,null,null, null,null);
        if(!(phones.getCount()<0)){
            while (phones.moveToNext()){
                String ident_broj=phones.getString(phones.getColumnIndex(ContactsContract.Contacts._ID));
                //kursor za mail
                Cursor mail1=x.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + "= ?",new String[]{ident_broj},null);
                int i =0;
                //get contacts names
                while(mail1.moveToNext()) {
                    i++;
                    String ime =mail1.getString(mail1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String email=mail1.getString(mail1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

                   if(mail1.getString(mail1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))!=null){
                        telefoncici_imena.add(ime);
                        telefoncici.add(new OsobaImenik(ime,email));
                        telefoncici_email.add(mail1.getString(mail1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)));
                    }


                }
                mail1.close();
            }
        }

     phones.close();
    }

    public interface OnItemClick1 {
        // TODO: Update argument type and name
        void onItemClicked(int pos);
    }

}