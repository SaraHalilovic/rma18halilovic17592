package com.ba.unsa.etf.rma.sara.rma18halilovic17592;

import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;


public class KategorijeAkt extends AppCompatActivity implements ListeFragment.OnItemClick1,MyListAdapter.OnChangeFragmentListener {
    public static final String EXTRA_TEXT = "kategorijice";
    public static final String EXTRA_KNJIGA = "knjigice";
    public static BazaOpenHelper help;
    ArrayList<String> unosi = new ArrayList<>();
   public static ArrayList<Knjiga> knjige = new ArrayList<>();
    ArrayList<String> autori = new ArrayList<>();


    FragmentManager fm;
    ListeFragment fl;


    public static Boolean siriL=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.activity_kategorije_akt);
        help= new BazaOpenHelper(this,"mojaBaza1.db", null, 1);


        siriL = false;

        DodavanjeKnjigeFragment d=new DodavanjeKnjigeFragment();
        FragmentManager fm=getFragmentManager();
        FrameLayout place2=(FrameLayout) findViewById(R.id.mjestoF2);
        FrameLayout place1=(FrameLayout)findViewById(R.id.mjestoF1);
        FrameLayout place3=(FrameLayout)findViewById(R.id.mjestoF3);
        FragmentManager manager = getFragmentManager();


        if(!(place2==null)) {
            siriL = true;
            KnjigeFragment kfragment = (KnjigeFragment)manager.findFragmentById(R.id.mjestoF2);
            if(kfragment==null){
                    kfragment=new KnjigeFragment();
            //if (d == null) {
                d = new DodavanjeKnjigeFragment();
                fm.beginTransaction().replace(R.id.mjestoF2, kfragment).commit();

            }
        }
 fl = (ListeFragment) manager.findFragmentById(R.id.FragmentPocetak);
        if (fl == null) {
            fl = new ListeFragment();
            Bundle argumenti = new Bundle();
            argumenti.putParcelableArrayList("Alista", knjige);
            fl.setArguments(argumenti);

            manager.beginTransaction().replace(R.id.FragmentPocetak, fl).commit();
        } else {
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void onItemClicked1 ( int pos){
//Priprema novog fragmenta FragmentDetalji
        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList("knjigice",knjige);
        KnjigeFragment fd = new KnjigeFragment();
        fd.setArguments(arguments);
//Slučaj za ekrane sa početno zadanom širinom
            getFragmentManager().beginTransaction()
                    .replace(R.id.FragmentPocetak, fd)
                    .addToBackStack(null)
                    .commit();
//Primijetite liniju .addToBackStack(null)
        }

    @Override
    public void changeFragment(Knjiga book) {

        FragmentPreporuci nextFrag= new FragmentPreporuci();
        Bundle bundle = new Bundle();
        //bundle.putString();
        bundle.putParcelable("knjigica_mala", book);
        nextFrag.setArguments(bundle);


        getFragmentManager().beginTransaction()
                .replace(R.id.FragmentPocetak, nextFrag, "findThisFragment")
                .addToBackStack(null)
                .commit();

    }
}


