package com.ba.unsa.etf.rma.sara.rma18halilovic17592;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public class Autor implements Parcelable{

    private int br_knjiga;
    private String imeiPrezime;
    private ArrayList<String>knjige=new ArrayList<>();



    public String getImeiPrezime() {
        return imeiPrezime;
    }

    public void setImeiPrezime(String imeiPrezime) {
        this.imeiPrezime = imeiPrezime;
    }




    public ArrayList<String> getKnjige() {
        return knjige;
    }

    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }


    public Autor(String ime, ArrayList<String>a){
        setImeiPrezime(ime);
        setKnjige(a);
        setBr_knjiga(1);
    }

    public Autor(String h){imeiPrezime=h;}

    public Autor(String naziv, int br){
        imeiPrezime=naziv;
        br_knjiga=br;
    }

    public void  dodajKnjigu(String id1){
        boolean da=false;
        for(int i=0;i<knjige.size();i++){
            if(id1.equals(knjige.get(i)))da=true;
        }
        if(!da){
            knjige.add(id1);
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imeiPrezime);

        String brojic = Integer.toString(br_knjiga);
        dest.writeString(brojic);
    }

    protected Autor(Parcel in){
        String brojic;
        brojic=in.readString();
        br_knjiga=Integer.parseInt(brojic);
        imeiPrezime=in.readString();


    }
public static final Creator<Autor> CREATOR=new Creator<Autor>() {
    @Override
    public Autor createFromParcel(Parcel in) {
        return new Autor(in);
    }

    @Override
    public Autor[] newArray(int size) {
        return new Autor[size];
    }
};

    public String getBr_knjiga() {
                return Integer.toString(br_knjiga);
    }

    public void setBr_knjiga(int br_knjiga) {
        this.br_knjiga = br_knjiga;
    }




    public Autor(){
       imeiPrezime="Sara";

        br_knjiga=5;
    }


        @Override
        public int describeContents() {
            return 0;
        }


}

