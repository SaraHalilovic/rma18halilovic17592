package com.ba.unsa.etf.rma.sara.rma18halilovic17592;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Boolean.FALSE;

public class MyListAdapter extends ArrayAdapter<Knjiga> {
    // declaring our ArrayList of items
    private Context mContext;
    private List<Knjiga> bookList = new ArrayList<>();
    private OnChangeFragmentListener m_onChangeFragmentListener;

    public interface OnChangeFragmentListener {
        void changeFragment(Knjiga book);
    }



    public MyListAdapter(@NonNull Context context, @SuppressLint("SupportAnnotationUsage") @LayoutRes ArrayList<Knjiga> list) {
        super(context, 0 , list);
        mContext = context;
        bookList = list;
        m_onChangeFragmentListener = (OnChangeFragmentListener) context;
    }
    @SuppressLint("ResourceAsColor")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.my_element,parent,false);

        final Knjiga currentKnjiga = bookList.get(position);

       // image.setImageResource(currentKnjiga.getmImageDrawable());
       /* try {
            image.setImageBitmap(BitmapFactory.decodeStream(this.getContext().openFileInput(currentKnjiga.getSlike())));
        }
        catch(FileNotFoundException ex) {
            ex.printStackTrace();
        }*/

        TextView name = (TextView) listItem.findViewById(R.id.eAutor);
        name.setText(currentKnjiga.getNaziv_Autora());


        TextView title = (TextView) listItem.findViewById(R.id.eNaziv);
        title.setText(currentKnjiga.getNaziv());

        TextView opis=(TextView) listItem.findViewById(R.id.eOpis);
        opis.setText(currentKnjiga.getOpis());

        TextView datum=(TextView) listItem.findViewById(R.id.eDatumObjavljivanja);
        datum.setText(currentKnjiga.getDatumObjavljivanja());

        TextView br_stranica=(TextView) listItem.findViewById(R.id.eBrojStranica);
        br_stranica.setText(String.valueOf(currentKnjiga.getBrojStranica()));

        ImageView slikica=(ImageView) listItem.findViewById(R.id.eNaslovna);
        if(currentKnjiga.getSlika()!=null){
        Picasso.get().load(currentKnjiga.getSlika().toString()).into(slikica);}
        else{
            Picasso.get().load(R.drawable.cikac1).into(slikica);
        }

        Button dugme_preporuci=listItem.findViewById(R.id.dPreporuci);
        dugme_preporuci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_onChangeFragmentListener.changeFragment(currentKnjiga);
            }
        });


       /* if(currentKnjiga.isKliknut()){
         listItem.setBackgroundColor(R.color.colorAccent);
        }*/
            return listItem;
    }
}





