package com.ba.unsa.etf.rma.sara.rma18halilovic17592;


import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import static android.app.DownloadManager.STATUS_RUNNING;
import static android.drm.DrmInfoStatus.STATUS_ERROR;

public class KnjigePoznanika extends IntentService
{

    Bundle bundle=new Bundle();
    ResultReceiver reciever;
    private ArrayList<Knjiga> knjigice ;

    public static int STATUS_START=0;
    public static int STATUS_FINISH=1;
    public static int STATUS_ERROR =2;


    @Override
    public void onCreate() {
        super.onCreate();
    }

public KnjigePoznanika(){
        super(null);
        knjigice=new ArrayList<Knjiga>();
  //      ids=new ArrayList<String>();
    }


public KnjigePoznanika(String name){
        super(name); }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        reciever = intent.getParcelableExtra("receiver");
        String query = intent.getStringExtra("idKorisnika");

        try {
            reciever.send(STATUS_START, Bundle.EMPTY);

            query = URLEncoder.encode(query, "utf-8");
            String urll = "https://www.googleapis.com/books/v1/users/" + query + "/bookshelves";
            URL url = new URL(urll);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream iStream = new BufferedInputStream(urlConnection.getInputStream());
            JSONObject objekat = new JSONObject(convertStreamToString(iStream));
            JSONArray itemsi = objekat.getJSONArray("items");
            ArrayList<String> listica = new ArrayList<>();

/*
            for(int j = 0; j < idKorisnik.size(); j++) {
                String url2 = "https://www.googleapis.com/books/v1/users/" + query + "/bookshelves/" + idKorisnik.get(j) + "/volumes";
                URL url3 = new URL(url2);
                HttpURLConnection urlConnection1 = (HttpURLConnection)url3.openConnection();
                InputStream novi_stream = new BufferedInputStream(urlConnection1.getInputStream());
                JSONObject objekatt = new JSONObject(convertStreamToString(novi_stream));
                JSONArray items = objekatt.getJSONArray("items");
                ArrayList<Knjiga> rez=new ArrayList<>();
                for (int p = 0; p < items.length(); p++) {
                    JSONObject knjigica = items.getJSONObject(j);
                    String idBroj;
                    if(knjigica.has("id")){
                        idBroj= knjigica.getString("id");}
                    else{ idBroj="0";}

                    //kreirati niz od 5 ovig knjigica :O


                    String naziv;
                    naziv= knjigica.getJSONObject("volumeInfo").getString("title");

                    // Log.i(TAG, "nemal"+ naziv);
                    String opis;
                    if(knjigica.getJSONObject("volumeInfo").has("description")){
                        opis=knjigica.getJSONObject("volumeInfo").getString("description");}
                    else{ opis ="";}

                    String dat;
                    if(knjigica.getJSONObject("volumeInfo").has("publishedDate")){
                        dat=knjigica.getJSONObject("volumeInfo").getString("publishedDate");}
                    else{
                        dat="";
                    }

                    int str=0;
                    if(knjigica.getJSONObject("volumeInfo").has("pageCount")){
                        String stranice=(knjigica.getJSONObject("volumeInfo").getString("pageCount"));
                        str=Integer.valueOf(stranice);}
                    //  Log.i(TAG, "stranice"+ str);

                    ArrayList<Autor> autori12=new ArrayList<>();
                    if(knjigica.getJSONObject("volumeInfo").has("authors")){
                        try{

                            JSONArray autorcici=knjigica.getJSONObject("volumeInfo").getJSONArray("authors");

                            for(int k=0;k<autorcici.length();j++){
                                ArrayList<String>e=new ArrayList<>();
                                e.add(idBroj);
                                Autor ig=new Autor(autorcici.get(j).toString(),e);
                                autori12.add(ig);

                                //   Log.i(TAG, "komada"+ ig.getImeiPrezime());
                            }}
                        catch (JSONException e) {
                            e.printStackTrace();
                        }}
                    else{
                        autori12.clear();
                    }






                    URL slikaa=null;
                    if(knjigica.getJSONObject("volumeInfo").has("imageLinks")) {
                        JSONObject iLinks = knjigica.getJSONObject("volumeInfo").getJSONObject("imageLinks");
                        if(iLinks.has("thumbnail")) {
                            String temp = iLinks.getString("thumbnail");
                            slikaa = new URL(temp);
                        }
                    }



                    //  ArrayList<Autor> autori12=new ArrayList<>();

                    Knjiga dohvacenaKnjiga=new Knjiga(idBroj,naziv,autori12,opis,dat,slikaa,str);
                    rez.add(dohvacenaKnjiga);



                }
                knjigice.addAll(rez);


            }*/

            bundle.putParcelableArrayList("lista", knjigice);
            reciever.send(STATUS_FINISH, bundle);
        }
           catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


    }

    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line;

        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line+"\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return sb.toString();
    }
}
