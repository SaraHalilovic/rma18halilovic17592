package com.ba.unsa.etf.rma.sara.rma18halilovic17592;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
public class ListaKnjigaAkt extends AppCompatActivity {
    ArrayList<Knjiga> knjigea = new ArrayList<Knjiga>();
    ListView lista_knjiga;
    ArrayList<Knjiga> knjigeJedan = new ArrayList<Knjiga>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_knjiga_akt);
        lista_knjiga = (ListView) findViewById(R.id.listaKnjiga);

        Button povratak = (Button) findViewById(R.id.dPovratak);

        final Intent in = new Intent();
        // final Bundle b = getIntent().getExtras();
        final Bundle bandl = getIntent().getExtras();
        knjigea = (ArrayList<Knjiga>) bandl.getSerializable("Listica");
        ArrayList<String> paa = new ArrayList<>();
        for (int i = 0; i < knjigea.size(); i++)
            paa.add(knjigea.get(i).getNaziv());

        MyListAdapter adapter = new MyListAdapter(ListaKnjigaAkt.this, knjigea);
        lista_knjiga.setAdapter(adapter);

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent resultIntent = new Intent();
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });

/*
* Knjiga Book= (Knjiga) lista_knjiga.getItemAtPosition(position);
               Book.setKliknut(true);
                */

        lista_knjiga.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                lista_knjiga.getItemAtPosition(position);
            }
        });
    }



}