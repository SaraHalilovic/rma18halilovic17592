package com.ba.unsa.etf.rma.sara.rma18halilovic17592;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class DohvatiNajnovije  extends AsyncTask<String,Integer,Void> {

    public  interface  IDohvatiNajnovijeDone​ {
        public void onNajnovijeDone(ArrayList<Knjiga> rez) ;
    }
    public static ArrayList<Knjiga> rez;
    private IDohvatiNajnovijeDone​  pozivatelj;
    public DohvatiNajnovije(IDohvatiNajnovijeDone​  p){pozivatelj=p;};


    @Override
    protected Void doInBackground(String... params) {


        String query = null;
        ArrayList<Knjiga> listaKnjiga = new ArrayList<Knjiga>();
        String API1="AIzaSyAws4x_2BkIFVELuNM6vegjlouL0qHbtwY";


        try {
            query = URLEncoder.encode(params[0], "utf-8");
            String url1 = "https://www.googleapis.com/books/v1/volumes?q=inauthor:" + query + "&orderBy=newest&maxResults=5";//"&key="+ API1;
            URL url = new URL(url1);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            if(in==null) return null;
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("items");

            for (int i = 0; i < items.length(); i++) {
                JSONObject knjigica = items.getJSONObject(i);
                String idBroj;
                if(knjigica.has("id")){
                    idBroj= knjigica.getString("id");}
                else{ idBroj="0";}

                //kreirati niz od 5 ovig knjigica :O


                String naziv;
                naziv= knjigica.getJSONObject("volumeInfo").getString("title");

                // Log.i(TAG, "nemal"+ naziv);
                String opis;
                if(knjigica.getJSONObject("volumeInfo").has("description")){
                    opis=knjigica.getJSONObject("volumeInfo").getString("description");}
                else{ opis ="";}

                String dat;
                if(knjigica.getJSONObject("volumeInfo").has("publishedDate")){
                    dat=knjigica.getJSONObject("volumeInfo").getString("publishedDate");}
                else{
                    dat="";
                }

                int str=0;
                if(knjigica.getJSONObject("volumeInfo").has("pageCount")){
                    String stranice=(knjigica.getJSONObject("volumeInfo").getString("pageCount"));
                    str=Integer.valueOf(stranice);}
                //  Log.i(TAG, "stranice"+ str);

                ArrayList<Autor> autori12=new ArrayList<>();
                if(knjigica.getJSONObject("volumeInfo").has("authors")){
                    try{

                        JSONArray autorcici=knjigica.getJSONObject("volumeInfo").getJSONArray("authors");

                        for(int j=0;j<autorcici.length();j++){
                            ArrayList<String>e=new ArrayList<>();
                            e.add(idBroj);
                            Autor ig=new Autor(autorcici.get(j).toString(),e);
                            autori12.add(ig);

                            //   Log.i(TAG, "komada"+ ig.getImeiPrezime());
                        }}
                    catch (JSONException e) {
                        e.printStackTrace();
                    }}
                else{
                    autori12.clear();
                }

                URL slikaa=null;
                if(knjigica.getJSONObject("volumeInfo").has("imageLinks")) {
                    JSONObject iLinks = knjigica.getJSONObject("volumeInfo").getJSONObject("imageLinks");
                    if(iLinks.has("thumbnail")) {
                        String mala_slikica = iLinks.getString("thumbnail");
                        slikaa = new URL(mala_slikica);
                    }
                }


                //  ArrayList<Autor> autori12=new ArrayList<>();


                Knjiga dohvacenaKnjiga=new Knjiga(idBroj,naziv,autori12,opis,dat,slikaa,str);
                listaKnjiga.add(dohvacenaKnjiga);



            }
            rez=listaKnjiga;
            //  Log.i(TAG, "komada"+ rezultat1.size());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;


    }

    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line;

        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line+"\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return sb.toString();
    }

    @Override protected  void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);

         pozivatelj.onNajnovijeDone(rez);


    }
}
