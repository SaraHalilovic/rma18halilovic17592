package com.ba.unsa.etf.rma.sara.rma18halilovic17592;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class Adapter_autori extends ArrayAdapter<Autor> {
    // declaring our ArrayList of items
    private Context mContext;
    private List<Autor> AutorList = new ArrayList<>();

    public Adapter_autori(@NonNull Context context, @SuppressLint("SupportAnnotationUsage") @LayoutRes ArrayList<Autor> list) {

        super(context,0, list);
        mContext = context;
        AutorList = list;
    }
    @SuppressLint("ResourceAsColor")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.autori_element,parent,false);

        Autor currentAutor = AutorList.get(position);

        ImageView image = (ImageView)listItem.findViewById(R.id.eNaslovna);
        // image.setImageResource(currentKnjiga.getmImageDrawable());
       /* try {
            image.setImageBitmap(BitmapFactory.decodeStream(this.getContext().openFileInput(currentKnjiga.getSlike())));
        }
        catch(FileNotFoundException ex) {
            ex.printStackTrace();
        }*/

        TextView name = (TextView) listItem.findViewById(R.id.ImenaA);
        name.setText(currentAutor.getImeiPrezime());

        TextView title1 = (TextView) listItem.findViewById(R.id.textView3);

        TextView title2 = (TextView) listItem.findViewById(R.id.textView4);


        TextView title = (TextView) listItem.findViewById(R.id.textView2);
        String p=currentAutor.getBr_knjiga();
        title.setText(p);


        return listItem;
    }
}


